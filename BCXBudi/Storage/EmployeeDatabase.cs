﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using BCXBudi.Tables;
using SQLite;

namespace BCXBudi.Storage
{
    public class EmployeeDatabase
    {
        readonly SQLiteAsyncConnection database;
        public string employeeNumber { get; set; }

        public EmployeeDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<EmployeeTable>().Wait();
        }

        public string GetProfilePicLocation() {
            return database.Table<EmployeeTable>().Where(i => i.EmployeeNumber == employeeNumber).FirstOrDefaultAsync().Result.ProfilePicLocation;
        }

        public Task<int> UpdateProfilePicLocation(string profilePicLocation) {
            EmployeeTable employeeTable = new EmployeeTable();
            employeeTable.EmployeeNumber = employeeNumber;
            employeeTable.ProfilePicLocation = profilePicLocation;
            return database.UpdateAsync(employeeTable);
        }

        public Task<int> UpdateProfile(EmployeeTable employeeTable)
        {
            return database.UpdateAsync(employeeTable);
        }

        public bool GetRememberMe() {
            Task<EmployeeTable> employeeTable = database.Table<EmployeeTable>().Where(i => i.RememberMe == true).FirstOrDefaultAsync();
            if (employeeTable == null || employeeTable.Result.RememberMe == false) {
                return false;
            }
            return true;
        }

        public Task<EmployeeTable> GetUsername()
        {
            return database.Table<EmployeeTable>().Where(i => i.EmployeeNumber == employeeNumber).FirstOrDefaultAsync();
        }

        public Task<EmployeeTable> GetProfile() 
        {
            return database.Table<EmployeeTable>().Where(i => i.EmployeeNumber == employeeNumber).FirstOrDefaultAsync();
        }

        public Task<int> InsertEmployeeInfoOnLogin(EmployeeTable empTable) {
            empTable.EmployeeNumber = employeeNumber;
            Task<EmployeeTable> employeeTable = database.Table<EmployeeTable>().Where(i => i.EmployeeNumber == employeeNumber).FirstOrDefaultAsync();
            if(employeeTable.Result == null) {
                return database.InsertAsync(empTable);
            }
            return null;
        }

        public Task<List<EmployeeTable>> GetItemsAsync()
        {
            return database.Table<EmployeeTable>().ToListAsync();
        }

        public Task<EmployeeTable> GetItemAsync()
        {
            return database.Table<EmployeeTable>().Where(i => i.EmployeeNumber == employeeNumber).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(EmployeeTable empTable)
        {
            if (empTable.EmployeeNumber != null)
            {
                return database.UpdateAsync(empTable);
            }
            else
            {
                return database.InsertAsync(empTable);
            }
        }

        public Task<int> DeleteItemAsync(EmployeeTable empTable)
        {
            return database.DeleteAsync(empTable);
        }
    }
}

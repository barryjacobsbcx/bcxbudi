﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using BCXBudi.Adapters;
using BCXBudi.Models;
using BCXBudi.Services;
using Xamarin.Forms;

namespace BCXBudi.Pages
{
    public partial class CanteenPage : ContentPage
    {
        public static ObservableCollection<CanteenItemList> Canteen { get; set; }

        public CanteenPage()
        {
            InitializeComponent();
            Canteen = new ObservableCollection<CanteenItemList>();
            BindingContext = this;
            SyncDataModel();
        }

        private async void SyncDataModel()
        {
            IRestFul<Task<Canteen>> canteenService = new CanteenService();
            Canteen canteen = new Canteen();
            //Children.Add(loadingPage);
            canteen = await canteenService.Get();
            foreach (CanteenItemList canteenEntry in canteen.canteenItemList)
            {
                Canteen.Add(canteenEntry);
            }
            CanteenList.ItemsSource = Canteen;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BCXBudi.Storage;
using BCXBudi.Tables;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace BCXBudi.Pages
{
    
    public partial class ProfilePage : ContentPage
    {
        public Task<EmployeeTable> employeeTable;
        public string Name { get; set; }
        public string Designation { get; set; }
        public Boolean EditMode { get; set; }

        public ProfilePage()
        {
            employeeTable = App.Database.GetProfile();
            if(App.Database.GetProfilePicLocation() != null) {
                profilePic.Source = App.Database.GetProfilePicLocation();
            }
            InflateProfile();
            BindingContext = this;
            InitializeComponent();

        }
        private async void EditProfile_Clicked(object sender, EventArgs e)
        {
            DesignationName.IsEnabled = true;
            DesignationName.Text = string.Empty;
            DesignationName.Placeholder = "Occupation";
            ProfileName.IsEnabled = true;
            ProfileName.Text = string.Empty;
            ProfileName.Placeholder = "Name & Surname";
            SaveProfile.IsVisible = true;
        }

        private async void SaveProfile_Clicked(object sender, EventArgs e)
        { 
            DesignationName.IsEnabled = false;
            ProfileName.IsEnabled = false;
            SaveProfile.IsVisible = false;
            EmployeeTable employeeTable = new EmployeeTable();
            employeeTable.EmployeeNumber = App.Database.employeeNumber;
            if(DesignationName.Text != string.Empty) {
                employeeTable.Designation = DesignationName.Text;
            } 
            if(ProfileName.Text != string.Empty) {
                employeeTable.Name = ProfileName.Text;
            }

            App.Database.UpdateProfile(employeeTable);
            InflateProfile();
        }

        private async void CameraButton_Clicked(object sender, EventArgs e)
        {
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                if (status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Camera))
                    {
                         DisplayAlert("Camera Permissions Needed", "Please try again", "OK");
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
                    //Best practice to always check that the key exists
                    if (results.ContainsKey(Permission.Location))
                        status = results[Permission.Location];
                }

                if (status == PermissionStatus.Granted)
                {
                    var photo = await Plugin.Media.CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions() { });
                    if (photo != null)
                    {
                        App.Database.UpdateProfilePicLocation(photo.AlbumPath);
                        profilePic.Source = photo.AlbumPath;
                    }
                }
                else if (status != PermissionStatus.Unknown)
                {
                    DisplayAlert("Camera Denied", "Can not continue, try again.", "OK");
                }
            }
            catch (Exception ex)
            {
                DisplayAlert("Camera Denied", "Can not continue, try again.", "OK");
            }
        }

        private void InflateProfile() {
            if (employeeTable.Result.Name == null)
            {
                Name = "Name & Surname";
            }
            else 
            {
                Name = employeeTable.Result.Name + " " + employeeTable.Result.Surname;
            }
            if (employeeTable.Result.Designation == null)
            {
                Designation = "Occupation";
            }
            else
            {
                Designation = employeeTable.Result.Designation;
            }
        }
    }
}

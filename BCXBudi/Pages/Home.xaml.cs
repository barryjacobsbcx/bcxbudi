﻿using System;
using System.Collections.ObjectModel;
using BCXBudi.Adapters;
using BCXBudi.Models;
using BCXBudi.Services;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Extensions;
using Xamarin.Forms;
using Rg.Plugins.Popup.Services;

namespace BCXBudi.Pages
{
    public partial class Home : ContentPage
    {
        NewsFeed newsFeed;
        public static ObservableCollection<NewsEntry> News { get; set; }
        private Grid grid;
        private Grid gridProgress;
        private ActivityIndicator activity;

        public Home()
        {
            InitializeComponent();
            News = new ObservableCollection<NewsEntry>();
            BindingContext = this;
            AddProgressDisplay(this);
            SyncDataModel();
            NewsList.ItemTapped += (object sender, ItemTappedEventArgs e) =>
            {
                PushPopup(sender, e);
            };
        }

        private async void PushPopup(object sender, ItemTappedEventArgs e) {
            var item = (NewsEntry)e.Item;
            await Navigation.PushModalAsync(new News(item.heading, item.body, item.imageUrl));
        }

        private async void SyncDataModel()
        {
            IRestFul<Task<NewsFeed>> newsService = new NewsService();
            NewsFeed newsFeed = new NewsFeed();
            //Children.Add(loadingPage);
            newsFeed = await newsService.Get();
            foreach (NewsEntry newsEntry in newsFeed.newsEntries) {
                News.Add(newsEntry);
            }
            NewsList.ItemsSource = News;
            grid.Children.Remove(gridProgress);
        }

        public void AddProgressDisplay(ContentPage page)
        {
            var content = page.Content;

            grid = new Grid();
            grid.Children.Add(content);
            gridProgress = new Grid { BackgroundColor = Color.FromHex("#64FFE0B2"), Padding = new Thickness(50) };
            gridProgress.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            gridProgress.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Auto) });
            gridProgress.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });
            gridProgress.SetBinding(VisualElement.IsVisibleProperty, "IsWorking");
            activity = new ActivityIndicator
            {
                IsEnabled = true,
                IsVisible = true,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                IsRunning = true
            };
            gridProgress.Children.Add(activity, 0, 1);
            grid.Children.Add(gridProgress);
            page.Content = grid;
        }
    }
}

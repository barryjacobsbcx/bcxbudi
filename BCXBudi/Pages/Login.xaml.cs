﻿using System;
using System.Collections.Generic;
using BCXBudi.Tables;
using Xamarin.Forms;

namespace BCXBudi.Pages
{
    public partial class Login : ContentPage
    {
        EmployeeTable employeeTable;

        public Login()
        {
            
            InitializeComponent();
            employeeTable = new EmployeeTable();
            EmployeePickerViewChange(false);
        }

        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            EmployeePickerViewChange(true);
        }

        private async void ReadMoreBtnClick(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new ReadMorePage());
        }
        private void SubmitBtnClick(object sender, EventArgs e)
        {
            if (usernameEntry.Text == "" || passwordEntry.Text == "")
            {
                messageLabel.Text = "Employee Number / Password Empty";
                passwordEntry.Text = string.Empty;
            }
            else
            {
                if (employeePicker.SelectedIndex == 1)
                {
                    App.Database.employeeNumber = usernameEntry.Text;
                    employeeTable.RememberMe = rememberMe.IsToggled;
                    App.Database.InsertEmployeeInfoOnLogin(employeeTable);
                    Application.Current.MainPage = new MainPage();
                }
                else if (employeePicker.SelectedIndex == 0)
                {
                    Application.Current.MainPage = new NewEmployeeMainPage();
                }

            }
        }

        private void EmployeePickerViewChange(bool selection) {
            usernameEntry.IsEnabled = selection;
            passwordEntry.IsEnabled = selection;
            submit.IsEnabled = selection;
        }
    }
}

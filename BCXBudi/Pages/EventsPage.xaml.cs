﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using BCXBudi.Adapters;
using BCXBudi.Models;
using BCXBudi.Services;
using Xamarin.Forms;

namespace BCXBudi.Pages
{
    public partial class EventsPage : ContentPage
    {
        public static ObservableCollection<EventEntry> Events { get; set; }

        public EventsPage()
        {
            InitializeComponent();
            Events = new ObservableCollection<EventEntry>();
            BindingContext = this;
            SyncDataModel();
            EventsList.ItemTapped += (object sender, ItemTappedEventArgs e) =>
            {
                PushPopup(sender, e);
            };
        }

        private async void PushPopup(object sender, ItemTappedEventArgs e)
        {
            var item = (EventEntry)e.Item;
            await Navigation.PushModalAsync(new Event(item.title, item.description, item.imageUrl, item.address, item.eventDate));
        }

        private async void SyncDataModel()
        {
            IRestFul<Task<EventList>> eventService = new EventsService();
            EventList eventList = new EventList();
            //Children.Add(loadingPage);
            eventList = await eventService.Get();
            foreach (EventEntry eventEntry in eventList.eventEntries)
            {
                Events.Add(eventEntry);
            }
            EventsList.ItemsSource = Events;
        }
    }
}

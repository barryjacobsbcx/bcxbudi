﻿using System;
namespace BCXBudi
{
    public class Constants
    {
        public static string NEWS_SERVICE_REST_URL = "https://bcxnewsfeedapi.azurewebsites.net/api/newsfeed";
        public static string CANTEEN_SERVICE_REST_URL = "https://bcxcanteenapi.azurewebsites.net/api/canteen/";
        public static string EVENT_SERVICE_REST_URL = "https://bcxeventsapi.azurewebsites.net/api/events/";
        public static string BLOB_STORAGE_REST_URL = "https://highvelocitystorage.blob.core.windows.net/employee-app/";
    }
}

﻿using System;
using SQLite;

namespace BCXBudi.Tables
{
    public class EmployeeTable
    {
        [PrimaryKey]
        public string EmployeeNumber { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Office { get; set; }
        public string Designation { get; set; }
        public bool RememberMe { get; set; }
        public string ProfilePicLocation { get; set; }
    }
}

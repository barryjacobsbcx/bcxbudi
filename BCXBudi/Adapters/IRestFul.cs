﻿using System;
namespace BCXBudi.Adapters
{
    public interface IRestFul<T>
    {
        T Get();
        void Post();
        void Put();
        void Delete();
        void Patch();
    }
}

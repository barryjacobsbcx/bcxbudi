﻿using System;
using System.Collections.Generic;
namespace BCXBudi.Models
{
    public class Canteen
    {
        public Canteen()
        {
        }
        public string transactionId { get; set; }
        public List<CanteenItemList> canteenItemList { get; set; }
    }
}

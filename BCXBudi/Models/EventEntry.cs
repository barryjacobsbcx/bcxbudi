﻿using System;
namespace BCXBudi.Models
{
    public class EventEntry
    {
        public string imageUrl { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string address { get; set; }
        public string eventDate { get; set; }
    }
}

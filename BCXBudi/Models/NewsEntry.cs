﻿using System;
namespace BCXBudi.Models
{
	public class NewsEntry
    {
        public NewsEntry()
        {
        }
        public String imageUrl { get; set; }
        public String heading { get; set; }
        public String body { get; set; }
        public String date { get; set; }
        public String views { get; set; }
    }
}
